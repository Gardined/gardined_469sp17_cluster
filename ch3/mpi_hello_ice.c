/* File:       
 *    mpi_hello.c
 *
 * Purpose:    
 *    A "hello,world" program that uses MPI
 *
 * Load module on cluster:
 *    module load rocks-openmpi 
 *    (NOTE: the actual MPI module may differ from one cluster to the next)
 *
 * Compile:    
 *    mpicc -g -Wall -std=c99 mpi_hello.c -o mpi_hello
 *
 * Usage: 
 * (NOTE: DO NOT USE THIS COMMAND ON THE CLUSTER HEAD NODE. 
 *        Instead, use a PBS script as demonstrated in class.)
 *    mpiexec -n <number of processes> ~/replace/with/path/to/mpi_hello
 *    (mpirun should also work, if you use -np instead of -n)
 *
 * Input:      
 *    None
 * Output:     
 *    A greeting from each process
 *
 * Algorithm short description:  
 *    Each process sends a message to process 0, which prints 
 *    the messages it has received, as well as its own message.
 *
 * IPP textbook:  Section 3.1 
 */

/* Header files and preprocessor directives */
#include <stdio.h>
#include <string.h>  /* For strlen             */
#include <mpi.h>     /* For MPI functions, etc */ 

const int MAX_STRING = 100;

int main(void) 
{
    char       greeting[MAX_STRING];  /* String storing message */
    int        comm_sz;               /* Number of processes    */
    int        my_rank;               /* My process rank        */

    /****************/
    /* Start up MPI */
    /****************/
    // MPI_Init() - Initializes the MPI execution environment
    // See http://www.mpich.org/static/docs/v3.1/www3/MPI_Init.html

    // For now, we won't use the pointers to the command line args,
    // and instead we will simply pass NULL for both. (Also, we'll see that it is
    // not always necessary to call MPI_Init from the main function.)
    MPI_Init( NULL, NULL ); 

    /*******************************/
    /* Get the number of processes */
    /*******************************/
    // In MPI, we have a "communicator" that represents a collection of
    // processes that can send messages to each other. Here, 
    // MPI_COMM_WORLD is a handle to the communicator that consists of all
    // processes started by the user at the beginning of program execution.
    //
    // The second argument to MPI_Comm_size is a pointer to the output 
    // integer variable comm_sz, which represents the number of processes
    // in MPI_COMM_WORLD. (Officially, the return type of MPI_Comm_size is
    // an int, but the return value is actually stored in the com_sz variable.)
    MPI_Comm_size( MPI_COMM_WORLD, /* in:  communicator, MPI_Comm */
                   &comm_sz);      /* out: */

    /***************************************/
    /* Get my rank amoung all the processes */
    /***************************************/
    // This is similar to MPI_Comm_size, but in this case, the return value
    // is the rank of the calling process in the communicator group
    // (here, in MPI_COMM_WORLD)
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 

    /*
    * Algorithm summary: 
    * ------------------
    * If the process is any rank other than zero:
    *
    *    Create a greeting message ( a formatted string), and 
    *    store in a string variable called greeting.
    *
    *    Send the greeting message to process zero
    *
    * Otherwise ( that is, if this IS the process at rank 0):
    *    
    *    Dislplay a greeting message for this process
    *
    *    For each of the remaining processes:
    *
    *       Receive the message contained in the greeting variable
    *
    *       Display the message   
    *
    */ 
    if (my_rank != 0) { 
        /******************/
        /* Create message */
        /******************/
        // Note: sprintf sends formatted output to the memory address 
        //   POINTED TO by greeting (greeting is an array, so it is a pointer)
        //   (we are not printing to stdout - not yet!)
        sprintf(greeting, "Greetings from process %d of %d!", my_rank, comm_sz);

        /*****************************/
        /* Send message to process 0 */
        /*****************************/
        // MPI_Send performs a "blocking send" of a message to another process. 
        // What this means is that the function may "block" (i.e., will not
        // return) until the message has been received by the destination process 
        // OR the message has been buffered by MPI on either the sender or 
        // receiver side.
        //
        // There are six arguments to MPI_Send (see p. 89 for the formal 
        //   parameter names)
        // 
        // 1) greeting corresponds to msg_buf_p, a pointer to the memory block
        //    containing the contents of the message to be passed.(Note that we 
        //    don't use the & here, because in C, a string IS an array, 
        //    and an array IS a pointer!) The block of memory storing the 
        //    message has a 'void' parameter type, which in this context means 
        //    it can accommodate a range of data types.
        //  
        // 2) strlen(greeting)+1 corresponds to msg_size, an integer value 
        //    representing the amount of data to be sent (The +1 is for \0), 
        //    which is the string terminator in C)
        // 
        // 3) MPI_CHAR is the msg_type,which is the datatype of the send buffer 
        //    element(Formally, the expected argument type is "MPI_Datatype" -- 
        //    see Table 3.1 for some of the MPI datatypes that are defined in mpi.h)
        // 
        // 4) The first zero corresponds to dest, the rank of the destination 
        //    process
        //
        // 5) The second zero corresponds to tag, a non-negative integer that 
        //    is used to distinguish messages that are otherwise identical but 
        //    are used for different purposes. For example, we might use 0 to 
        //    tag a value for printing (to stdout), while a tag of 1 might 
        //    indicate that the value could be used for computation
        // 
        // 6) MPI_COMM_WORLD, discussed above, corresponds to the 
        //    communicator handle
        //
        // ** Note that all six of the above arguments are INPUT parameters. **
        //
        // Additional details can be found in the MPI documentation -- e.g., 
        // http://www.mpich.org/static/docs/latest/www3/MPI_Send.html 
        MPI_Send( greeting,           /* msg_buf_p (_p for pointer), void */
                  strlen(greeting)+1, /* msg_size, int */
                  MPI_CHAR,           /* msg_type, MPI_Datatype */
                  0,                  /* dest, int */
                  0,                  /* tag, int */
                  MPI_COMM_WORLD);    /* communicator, MPI_Comm */
    }
    else // if I AM the master core (if my_rank == 0)
    {  
        /********************/
        /* Print my message */
        /********************/
        printf("Greetings from process %d of %d!\n", my_rank, comm_sz);

        /**************************************************/
        /* Loop thru other cores and orint their messages */
        /**************************************************/
        for (int q = 1; q < comm_sz; q++) 
        {
            /**********************************/  
            /* Receiving message from process q */
            /**********************************/
            // Working in conjunction, MPI_Recv performs a 
            // "blocking receive" for a given message, which means that it does 
            // not return until the receive buffer has been populated with 
            // valid data.
            // 
            // MPI_Recv has a total of 7 arguments (5 input, 2 output), 
            //   described as follows:
            //
            // 1) greeting, as above, corresponds to msg_buf_p, the pointer  
            //    to the block of memory where the message is stored Unlike 
            //    in MPI_Send, this is an *output* parameter.
            //
            // 2) MAX_STRING corresponds to buf_size, the number of elements 
            //    that can be stored in the block (the receive buffer). Note 
            //    that MAX_STRING was defined above as a constant integer with a
            //    value of 100 (though the value can certainly vary.)
            // 
            // 3) MPI_CHAR corresponds to buf_type, the "MPI_DataType" of each 
            //    buffer element
            // 
            // 4) q corresponds to source(the interger rank of the process that 
            //    SENT the messag)
            // 
            // 5) The zero value is the integer tag, which should match the tag
            //    argument value of the message being sent (in this case, it 
            //    matches the 0 tag in the MPI_Send argument list)
            //
            // 6) MPI_COMM_WORLD is the handle to our communicator (obviously 
            //    should match the handle used in MPI_Send)
            // 
            // 7) The last argument, MPI_STATUS_IGNORE, is the other *output* 
            //    argument (status_p). This is a pointer to an MPI_Status object
            //    (in C, a struct) that provides information about the amount of
            //    data in the message, the sender (source) of the message, the 
            //    tag value, and the "error code" associated with the 
            //    communication. (If we aren't interested in this information, 
            //    we can pass MPI_STATUS_IGNORE as shown here.)
            //
            MPI_Recv( greeting,             /* out: msg_buf_p, void */
                      MAX_STRING,           /* in:  buf_size,  */
                      MPI_CHAR,             /* in:  buf_type,  */ 
                      q,                    /* in:  source, int  */
                      0,                    /* in:  tag, int  */
                      MPI_COMM_WORLD,       /* in:  communicator, MPI_Comm  */
                      MPI_STATUS_IGNORE );  /* out: status_p, MPI_Status */

            /* Print message from process q */
            printf( "%s\n", greeting );

        } // end for

    } // end if-else

    /*****************/
    /* Shut down MPI  */
    /*****************/
    // This will free up any resources that were allocated for MPI. In general,
    // NO MPI functions should be called after the call to MPI-Finalize!
    // (In addition, it's not always necessary to call MPI_Finalize from the 
    // main function, though in many cases that is what we'll be doing.)
    MPI_Finalize(); /* terminates the MPI execution environment */

    return 0; // the return type of main was int, so we must return a value

}  /* end main */
