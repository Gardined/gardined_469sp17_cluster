/* File:    trap2mpi.c
 * Purpose: Calculate definite integral using trapezoidal 
 *          rule.
 *
 * Input:   a, b, n
 * Output:  Estimate of integral from a to b of f(x)
 *          using n trapezoids.
 *
 * Compile: mpicc -g -Wall -o trap2mpi trap2mpi.c
 * Usage:   mpiexec -n <# of processes> ./trap2mpi
 *
 * Note:    The function f(x) is hardwired.
 *
 * IPP:     Section 3.2.1 (pp. 94 and ff.) and 5.2 (p. 216)
 */

#include <stdio.h>
#include <mpi.h> 

double f(double x);    /* Function we're integrating */
double Trap(double a, double b, int n, double h);

int main(void) {

    /* define MPI variables */

    int my_rank, comm_sz, n=1024, local_n;
    double a=0.0, b=3.0, h;
    double local_a, local_b ; // integration limits for each process
    double local_int; // local integration sum
    double total_int; // accumulator variable to store integration sum
    int source;


   // double  integral;   /* Store result in integral   */
   //double  a, b;       /* Left and right endpoints   */
   //int     n;          /* Number of trapezoids       */
   //double  h;          /* Height of trapezoids       */

   //printf("Enter a, b, and n\n");
   //scanf("%lf", &a);
   //scanf("%lf", &b);
   //scanf("%d", &n);

    //Start up MPI 
    MPI_Init( NULL, NULL);

    // get my process's rank 
    MPI_Comm_rank( MPI_COMM_WORLD, &my_rank );
    

    // Find out how many processes are being used
    MPI_Comm_size( MPI_COMM_WORLD, &comm_sz );

    // Always let the master core be responsible for Input/Output
    
    //if ( my_rank == 0 )
    //{


    //} // end if
    
    // diagnostic output
    printf("Core %d thinks a = %f\n, b = %f\n", my_rank, a , b);

    // here is where the parallel work actually begins
   h = (b-a)/n; // h will be the same for all processes
    local_n = n/comm_sz; // so is the number of trapezoids 

    // note that the length of each process's interval of 
    // integration will be equal to local_n * h 
    // thus "my" interval is computed as follows:
    local_a = a + my_rank * (local_n * h);
    local_b = local_a + (local_n * h);

   //integral = Trap(a, b, n, h);
    local_int = Trap( local_a, local_b, local_n, h);

    if ( my_rank != 0 ) // if I'm not the master core....
    {
        // send my result to the master core
        MPI_Send( &local_int, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD );
    } 
    else // if I AM the master core....
    {
        // go ahead and add "my" local_int to the total_int
        total_int = local_int; 

        // receive and add
        for ( source = 1; source < comm_sz; source++ )
        {
            MPI_Recv( &local_int, 1, MPI_DOUBLE, source, 0, MPI_COMM_WORLD,
                    MPI_STATUS_IGNORE );
            total_int += local_int;
        } // end for         

    } // end if/else


    // use the master process ( core 0 ) to print results to console output
    if ( my_rank == 0 )
    {

        printf("With n = %d trapezoids, our estimate\n", n);
        printf("of the integral from %f to %f = %.15f\n",
            a, b, total_int);
    }// end if 

    // shut down MPI
    MPI_Finalize(); 

   return 0;
}  /* main */

/*------------------------------------------------------------------
 * Function:    Trap
 * Purpose:     Estimate integral from a to b of f using trap rule and
 *              n trapezoids
 * Input args:  a, b, n, h
 * Return val:  Estimate of the integral 
 */
double Trap(double a, double b, int n, double h) {
   double integral;
   int k;

   integral = (f(a) + f(b))/2.0;
   for (k = 1; k <= n-1; k++) {
     integral += f(a+k*h);
   }
   integral = integral*h;

   return integral;
}  /* Trap */

/*------------------------------------------------------------------
 * Function:    f
 * Purpose:     Compute value of function to be integrated
 * Input args:  x
 */
double f(double x) {
   double return_val;

   return_val = x*x;
   return return_val;
}  /* f */
