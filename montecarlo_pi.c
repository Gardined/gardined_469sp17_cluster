#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

/* 
 * File: montecarlo_pi.c
 *
 * Purpose: compute an estimate of the value of Pi using 
 *          the Monte Carlo Method
 *
 * To Compile : 
 *
 * gcc -g -wall -lm montecarlo_pi.c -o montecarlo_pi.exe
 *
 *
 * Notes: 
 *
 * argc = argument count ( # of arguments, including the command itself)
 * 
 * argv = argument vectors ( 1-D array of strings)
 *
 * If we run the program using
 *
 *
 * ./montecarlo_pi.exe <number_of_iterations> 
 *
 * then argc = 2 , argv[0] = the executable file 
 *  and argv [1] = number of iterations 
 *
 */

int main( int argc, char* argv[])
{
    double x, y; // coordinates of each point
    int total_points = atoi( argv[1]); // converts string to int 
    int i; // counter variable for for loop 
    int points_in_circle = 0;
    double z; // test calue to see if point (x,y) is in circle 
    double pi; 

    srand( time(NULL) ); // seed random number generator using system clock 

    // generate randomly located points in a 2x2 square
    // in which a unit circle (of radius 1) has been inscribed
    
    for ( i = 0; i < total_points; i++ )
    {
        // get random point (x,y) inside the 1st quadrant of the square
        
        x = 2.0*(double)rand()/RAND_MAX - 1.0; // 0 < x < +1
        y = 2.0*(double)rand()/RAND_MAX - 1.0; // 0 < y < +1

        // next we will need to see if the point (x,y) 
        // lies within the unit circle inscribed in the square 

        z= sqrt( x*x + y*y); 

        // if the sqrt of x^2 + y^2 <= 1, then we count that point 
        // as being inside the ( first quadrant of) unit circle
        

        if ( z <= 1 )
        {
            points_in_circle++;
 
        }// end if statement
            
        }
        /* 
         *  If we let P refer to the radio points inside the cirlce 
         *  to the total number of points, then if the number of points 
         *  is large enough, P approaches the are of a unit circle
         *  (A = pi* r^2, with r = 1) divided by the are of the 2x2 square
         *  in which the circle is inscribed ( A = (2r)^2 = 4*r^2, r = 1).
         *  This ratio evaluates to pi/4, which when multiplied by 4
         *  results in our Monte Carlo estimate of pi.
         */

        pi = 4.0 * ((double)points_in_circle / total_points);

        // output result
        printf( " Pi: %f\n", pi);

        return 0;       

} // end main 
